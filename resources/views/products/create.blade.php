@extends('dashboard.base')

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Error!</strong> 
            <ul>
                @foreach ($errors->all() as $error)
                    <li></li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Create Product') }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('products.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label>Name</label>
                                <input class="form-control" type="text" name="name" placeholder="{{ __('Name') }}" required autofocus>
                            </div>

                            <div class="form-group row">
                                <label>Description</label>
                                <textarea class="form-control" id="textarea-input" name="description" rows="9" placeholder="{{ __('Description..') }}" require></textarea>
                            </div>

                            <div class="form-group row">
                                <label>Image</label>
                                <input class="form-control" type="file" name="image" placeholder="{{ __('Image') }}" required autofocus>
                            </div>

                            <div class="form-group row">
                                <label>Price</label>
                                <input type="number" name="price" class="form-control" placeholder="{{ __('Price') }}" step="0.01" require>
                            </div>
 
                            <button type="submit" class="btn btn-block btn-success" type="submit">{{ __('Add') }}</button>
                            <a href="{{ route('products.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>



@endsection

@section('javascript')

@endsection