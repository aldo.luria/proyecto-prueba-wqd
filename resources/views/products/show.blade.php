@extends('dashboard.base')


@section('content')

    <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> <h4>Product: {{ $product->name }}</h4></div>
                    <div class="card-body">
                        <center>
                        <img src="{{ Storage::url($product->image) }}" height="200" width="300" alt="" />
                        </center>
                        <h4>Price:</h4>
                        <p>$ {{ $product->price }}</p>
                        <h4>Description:</h4>
                        <p>{{ $product->description }}</p>
                        <h4>Created:</h4> 
                        <p>{{ $product->created_at }}</p>
                        <h4>Updated at:</h4>
                        <p>{{ $product->updated_at }}</p>
                        <a href="{{ route('products.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>

@endsection


@section('javascript')

@endsection