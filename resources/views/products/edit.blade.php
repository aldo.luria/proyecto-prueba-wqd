@extends('dashboard.base')

@section('content')

    <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-10 col-lg-8 col-xl-6">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i> {{ __('Edit') }}: {{ $product->name }}</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('products.update',$product->id) }}" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div class="form-group row">
                                <div class="col">
                                    <label>Name</label>
                                    <input class="form-control" type="text" name="name" placeholder="{{ __('Name') }}" value="{{ $product->name }}" required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label>Description</label>
                                    <textarea class="form-control" id="textarea-input" name="description" rows="9" placeholder="{{ __('Description..') }}" require>{{ $product->description }}</textarea>
                                </div>
                            </div>

                            <img src="{{ Storage::url($product->image) }}" height="200" width="200" alt="" />
                            <div class="form-group row">
                                <div class="col">
                                    <label>Image</label>
                                    <input class="form-control" type="file" name="image" placeholder="{{ __('Image') }}" value="{{ Storage::url($product->image) }}" required>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col">
                                    <label>Price</label>
                                    <input type="number" name="price" class="form-control" placeholder="{{ __('Price') }}" value="{{ $product->price }}" step="0.01" require>
                                </div>
                            </div>
 
                            <button type="submit" class="btn btn-block btn-success" type="submit">{{ __('Save') }}</button>
                            <a href="{{ route('products.index') }}" class="btn btn-block btn-primary">{{ __('Return') }}</a> 
                        </form>
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>



@endsection

@section('javascript')

@endsection