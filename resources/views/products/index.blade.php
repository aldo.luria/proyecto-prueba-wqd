@extends('dashboard.base')

@section('content')

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <center>{{ $message }}</center>
        </div>
    @endif
        <div class="container-fluid">
          <div class="animated fadeIn">
            <div class="row">
              <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-header">
                      <i class="fa fa-align-justify"></i>{{ __('Products') }}</div>
                    <div class="card-body">
                        <div class="row"> 
                          <a href="{{ route('products.create') }}" class="btn btn-primary m-2">{{ __('Add Product') }}</a>
                        </div>
                        <br>
                        <table class="table table-responsive-sm table-striped">
                        <thead>
                          <tr>
                            <th>No</th>
                            <th>Image</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Date Created</th>
                            <th></th>
                            <th></th>
                            <th></th>
                          </tr>
                        </thead>
                        <tbody>
                        @foreach ($products as $product)

                            <tr>
                              <td><strong>{{ $product->id }}</strong></td>
                              <td><img src="{{ Storage::url($product->image) }}" height="50" width="50" alt="{{ $product->name }}" /></td>
                              <td><strong>{{ $product->name }}</strong></td>
                              <td>{{ $product->description }}</td>
                              <td>$ {{ $product->price }}</td>
                              <td>{{ $product->created_at }}</td>
                              <td>
                                <a href="{{ url('/products/' . $product->id) }}" class="btn btn-block btn-primary">View</a>
                              </td>
                              <td>
                                <a href="{{ url('/products/' . $product->id . '/edit') }}" class="btn btn-block btn-primary">Edit</a>
                              </td>
                              <td>
                                <form action="{{ route('products.destroy', $product->id ) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button class="btn btn-block btn-danger">Delete</button>
                                </form>
                              </td>
                            </tr>
                          @endforeach
                        </tbody>
                      </table>
                      {!! $products->links() !!}
                    </div>
                </div>
              </div>
            </div>
          </div>
        </div>


@endsection

@section('javascript')

@endsection
