@extends('dashboard.authBase')

<html>
<title>We Qode Developers</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<head>
<style>
body,h1 {font-family: "Raleway", sans-serif}
body, html {height: 130%;
color: #A7A8AA !important;}
.links > a {
                color: #6A6DCD !important;
                padding: 0 25px !important;
                font-size: 13px !important;
                font-weight: 600 !important;
                letter-spacing: .1rem !important;
                text-decoration: none !important;}
</style>
</head>

<body style="background-color: #201547;">
    <div class="container text-center">
        <div class="row">
            <div class="w3-display-middle" align="center">
                <h1 class="w3-jumbo w3-animate-top">
                <img src="{{ URL('/assets/brand/Recurso_1.png') }}" alt="/" style="width:80%;">
                </h1>
                <br>
                <hr class="w3-border-grey" style="margin:auto;width:40%">
                <br>
                <p class="w3-large w3-center">Somos una empresa de Software!</p>
                <div class="flex-center position-ref full-height">
                        @if (Route::has('login'))
                            <div class="top-right links">
                                @auth
                                    <a href="{{ url('/dash') }}">Dashboard</a>
                                @else
                                    <a href="{{ route('login') }}">Login</a>

                                    @if (Route::has('register'))
                                        <a href="{{ route('register') }}">Register</a>
                                    @endif
                                @endauth
                            </div>
                        @endif
                </div>
            </div>
        </div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
        <div class="row">
            <div class="col-lg-4">
                <i class="fas fa-tv m-auto text-secondary" style='font-size:36px'></i>
                 <br>
                <h3>Fully Responsive</h3>
                <p class="lead mb-0">This theme will look great on any device, no matter the size!</p>
 
            </div>

            <div class="col-lg-4">
                <i class="fa fa-file-text-o m-auto text-secondary" style='font-size:36px'></i>
                <br>
                <h3>Bootstrap 4 Ready</h3>
                <p class="lead mb-0">Featuring the latest build of the new Bootstrap 4 framework!</p>
            </div>

            <div class="col-lg-4">
                <i class="fa fa-check-square-o m-auto text-secondary" style='font-size:36px'></i>
                 <br>
                <h3>Easy to Use</h3>
                <p class="lead mb-0">Ready to use with your own content, or customize the source files!</p>
           
            </div>
        </div><br><br><br><br><br><br>

        <div class="row">
            <div class="col">
            </div>

            <div class="col-lg-8">
                <i class="fa fa-handshake-o m-auto text-secondary" style='font-size:36px'></i>
                <br>
                <h3>Join the Team </h3>
                <p class="lead mb-0">Register now!</p><br>
                <a href="{{ route('register') }}" class="btn btn-secondary"> <i class="fa fa-pencil"></i> Register</a>
            </div>

            <div class="col">
            </div>
        </div>
        

    </div>
</body>
</html>
@section('javascript')

@endsection